<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Throwable;

trait JsonResponse
{
    private $errorMessage = 'Technical error. Please try again Later';
    /**
     * Success resposnse generation function
     *
     * @author Siji
     *
     * @param array $data - data to be sent in the response
     * @param string $message - message to be sent with response
     * @param integer $statusCode - status code of the response
     * @param array $headers - advance headers sent with headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSuccessResponse($data = [], $message = 'Success', $statusCode = 200, $headers = [])
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
            'code' => $statusCode
        ], $statusCode)->withHeaders($headers);
    }

    /**
     * Error resposnse generation function
     *
     * @author Siji
     *
     * @param array $data - data to be sent in the response
     * @param string $message - message to be sent with response
     * @param integer $statusCode - status code of the response
     * @param array $headers - advance headers sent with headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendErrorResponse($data = [], $message = '', $statusCode = 404, $headers = [])
    {
        DB::rollback();
        Log::info($message . ' - ' . $statusCode);
        Log::info($data);
        if ($message instanceof Throwable) {
            list($message, $statusCode, $headers) = $this->errorResponseGeneration($message, $statusCode, $headers);
        } else if ($message == '') {
            $message = $this->errorMessage;
        }

        if (app()->isProduction() && $statusCode == 500) {
            // Sending common message in case of 500 errors.
            // This is to avoid sql queries being returned to the user in the exception messages
            $message = $this->errorMessage;
        } else if ($statusCode == 0) {
            $statusCode = 500;
        }

        return response()->json([
            'success' => false,
            'message' => $message,
            'errors' => $data,
            'code' => $statusCode
        ], $statusCode)->withHeaders($headers);
    }


    /**
     * Generate messages, header and status code if exception detected
     *
     * @author Siji
     *
     * @param Symfony\Component\ErrorHandler $excepetion - Exception throwed
     * @param integer $statusCode - Custom status code
     * @param array $headers - Custom headers
     * @return array - Generated response data such as message, headers and status code
     */
    private function errorResponseGeneration($excepetion, $statusCode = 404, $headers = [])
    {
        $exceptionHeaders = [];
        $message = $excepetion->getMessage();

        if ($excepetion instanceof HttpException) {
            $exceptionHeaders = $excepetion->getHeaders();
            $headers = array_merge($headers, $exceptionHeaders);
        }

        if ($excepetion instanceof AuthenticationException) {
            $statusCode = 401;
            $message = 'Unauthorised request';
        } else if ($excepetion instanceof ModelNotFoundException) {
            $statusCode = 404;
            $message = 'Resource not found';
        } else if ($excepetion instanceof TokenMismatchException) {
            $statusCode = 419;
            $message = 'Page expired. Please refresh the page and try again';
        } else if ($excepetion instanceof TooManyRequestsHttpException) {
            $statusCode = 429;
            // we check if there is a Retry-After Header
            // Usually this header will contain the number of seconds
            // the user needs to wait before they are unblocked
            if (isset($exceptionHeaders['Retry-After']) && $exceptionHeaders['Retry-After']) {
                $seconds = $exceptionHeaders['Retry-After'];
                $message = 'We have received too many requests from your side. Please wait for ' . $seconds . ' seconds and try again';
            }
        }

        return [$message, $statusCode, $headers];
    }
}
