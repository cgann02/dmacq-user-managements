<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect( ( $guard == 'admin' ? RouteServiceProvider::ADMIN_DASHBOARD : RouteServiceProvider::HOME ));
            }
        }

        return $next($request);
    }
}
