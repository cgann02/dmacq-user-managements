<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticatedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $guard = 'user')
    {
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }

        if (Auth::guard($guard)->check()) {
            if($request->ajax()) {
                return response()->json(['status' => false, 'msg' => "Something went wrong!"], 411);
            }
        } else {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
