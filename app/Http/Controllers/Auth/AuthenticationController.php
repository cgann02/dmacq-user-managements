<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\PostLogin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class AuthenticationController extends Controller
{
    public function login()
    {
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }

        if(Auth::guard('admin')->user()) {

            return redirect()->route('admin.dashboard');

        } else if(Auth::guard('user')->user()) {
            
            return redirect()->route('user.dashboard');

        }
        return view('auth.login');
    }

    public function postLogin(PostLogin $request)
    {
        $credential = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $role = $request->input('role');

        if ($role == 'admin' && Auth::guard('admin')->attempt($credential)) {
            $request->session()->regenerate();
            
            return redirect()->route('admin.dashboard');
        } else if ($role == 'user' && Auth::guard('user')->attempt($credential)) {
            $request->session()->regenerate();
            
            return redirect()->route('user.dashboard');
        } else {
            return back()
                ->withInput()
                ->with('commonError', "Incorrect user login details!");
        }
    }
}
