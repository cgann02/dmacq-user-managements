<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth_user');
    }

    /*
     * After logging as user the dashboard for user
     * @return \Illuminate\Contracts\Support\Referable
     * */
    public function userDashboard()
    {
        return view('user.dashboard');
    }

    /**
     * Logout function
     *
     * @return void
     */
    public function userLogout()
    {
        Auth::guard('user')->logout();
        return redirect()->route('login');
    }
}
