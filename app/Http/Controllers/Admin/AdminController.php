<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UsersListDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOrUpdateUserDetailsRequest;
use App\Models\User;
use App\Repositories\AdminRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->middleware('auth_admin');
    }

    /*
     * After logging as admin the dashboard for admin
     * @return \Illuminate\Contracts\Support\Referable
     * */
    public function adminDashboard()
    {
        return view('admin.dashboard');
    }

    public function index(UsersListDataTable $dataTable)
    {
        return $dataTable->render('admin.users-list');
    }

    /**
     * creating new user form loading
     */

    public function getNewUserForm()
    {
        return view('admin.create-user');
    }

    public function storeUserDetails(StoreOrUpdateUserDetailsRequest $request)
    {
        try {

            $user = $this->adminRepository->createNew($request->all());

            return response()->json([
                'success' => true,
                'message' => __('form-elements.user_created_successfully'),
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            dd($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }

    public function getUpdateUserForm($id)
    {
        $user = User::find($id);
        if($user) {

            return View('admin.edit-user', compact('user'));
        }
        
        return back()->with('commonError', __('form-elements.user_data_not_found'));
    }

    public function updateUserDetails (StoreOrUpdateUserDetailsRequest $request)
    {
        try {

            $user = $this->adminRepository->updateUserInfo($request->user_id, $request->all());

            return response()->json([
                'success' => true,
                'message' => __('form-elements.user_details_updated_successfully'),
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }

    public function deleteUser($id)
    {
        try {

            $user = User::find($id)->delete();

            return response()->json([
                'success' => true,
                'message' => __('form-elements.user_deleted_successfully'),
                'data' => $user,
                'code' => 200
            ], 200);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => [],
                'code' => $e->getCode()
            ], $e->getCode());
        }
    }
    /**
     * Logout function
     *
     * @return void
     */
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('login');
    }
}
