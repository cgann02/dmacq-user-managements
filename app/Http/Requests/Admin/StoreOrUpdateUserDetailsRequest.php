<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;

class StoreOrUpdateUserDetailsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $validation = [
            'name' => 'required|string|max:100',
            'status' => 'required|in:0,1'
        ];

        if(!$this->input('user_id')) {
            $validation['email']      = 'required|email|unique:users,email';
        } else {
            $validation['email']      = 'required|email|unique:users,email,'.$this->input('user_id');
        }

        return $validation;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'        => __('extra-validation.field_required'),
            'email.required'       => __('extra-validation.field_required'),
            'status.required'      => __('extra-validation.field_required'),
            'status.in'            => __('extra-validation.value_in'),
            'name.string'          => __('extra-validation.field_required'),
            'name.max'             => __('extra-validation.field_required'),
            'email.email'          => __('extra-validation.valid_email'),
            'email.unique'         => __('extra-validation.value_unique')
        ];
    }
}
