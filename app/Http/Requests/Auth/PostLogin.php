<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class PostLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $role = $this->input('role') ?: null ;

        if(!$role) {
            return [
                'role' => 'bail|required|in:admin,user'
            ];
        }

        if($role == 'admin') 
        {
            return [
                'email' => 'bail|required|email|exists:admins,email',
                'password' => 'required'

            ];
        } else {

            $validateInput =  [
                'email' => 'bail|required|email|exists:users,email',
                'password' => 'required'
            ];
        }

        return $validateInput;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }

        return [
            'email.required'    => __('extra-validation.field_required'),
            'password.required' => __('extra-validation.field_required'),
            'role.required'     => __('extra-validation.field_required'),
            'role.in'           => __('extra-validation.value_in'),
            'email.email'       => __('extra-validation.valid_email'),
            'email.exists'      => __('extra-validation.value_exists')
        ];
    }
}
