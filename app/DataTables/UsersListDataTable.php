<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsersListDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
        ->eloquent($query)
        ->editColumn('name', function ($row) {
            
            return $row->name;
        })
        ->editColumn('Status', function ($row) {
            $class = 'secondary';
            $statusText = __('form-elements.inactive_user');
            if($row->status == 1) {
                $class = 'success';
                $statusText = __('form-elements.active_user');
            }
            return '<span class="badge badge-sm bg-gradient-'. $class . '">' . $statusText . '</span>';
        })
        ->addColumn('action', function ($row) {
            return view('components.datatable_actions', ['id' => $row->id, 'path' => 'admin.edit-user']);
        })
        ->addIndexColumn()
        ->rawColumns(['Status', 'action', 'name']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()
            ->select(
                'id',
                'name',
                'email',
                'status'
            );
            // ->except(Auth::guard('user')->id());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->setTableId('user-details-datatable')
        ->columns($this->getColumns())
        // ->ajax([
        //     'data' => 'function(d) {
        //         d.age = $("#user-age").val() ? $("#user-age").val() : -1;
        //     }',
        // ])
        ->dom("<'row'<'col-sm-6'B><'col-sm-6 text-right'l>>rt<'row'<'col-sm-6'p><'col-sm-6 text-right'i>>")
        ->orderBy(2, 'asc')
        ->buttons($this->generateButtons())
        ->parameters([
            "pagingType" => "simple_numbers",
            "language" => [
                'paginate' => [
                    'previous' => '<span aria-hidden="true" class="material-icons">chevron_left</span><span>Prev</span>',
                    'next' => 'NEXT<span aria-hidden="true" class="material-icons">chevron_right</span>',
                ],
                'info' => '_START_ <span class="text-50">of _TOTAL_ <em class="material-icons ml-1">arrow_forward</em></span>',
                'infoFiltered' => '',
                "infoEmpty" => "<span class='text-50'>No records available</span>",
            ],
            'initComplete' => "function () {
                let oTable = $('#user-details-datatable').DataTable(),
                    page_element = {
                        search: $('#filter-search'),
                        filter: $('#filter'),
                        status:  $('#filter-status')
                    }

                // global search
                page_element.search.on('keyup', function() {
                    let keyword = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    oTable.column(1).search(keyword).draw() ;
                });

                // status dropdown filter event
                page_element.status.on('change', function() {
                    let status_value = $(this).val();
                    oTable.column(3).search( status_value ? status_value : ' ', true, false )
                    .draw();
                });
                
           }",
            'responsive' => 'true',
            'drawCallback' => "function () {
                $('.dataTables_paginate > .pagination').addClass('justify-content-start pagination-xsm m-0');
            }",
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                ->title('#')
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->orderable(false),
                'Name' => ['data' => 'name', 'name' => 'name'],
                'Email' => ['data' => 'email', 'name' => 'email', 'search' => false],
                'Status' => ['data' => 'Status', 'name' => 'status'],
                Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_list' . date('YmdHis');
    }

    /**
     * Generate buttons
     *
     * @author Siji <cgann02@gmail.com>
     *
     * @return array
     */
    protected function generateButtons(): array
    {
        return [
            Button::make('reset')
        ];
    }
}
