<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class AdminRepository extends BaseRepository

{
    private $userDetail;
    /**
     * AdminRepository constructor.
     *
     * @author Siji <cgann02@gmail.com>
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * Create model data
     *
     * @param [array | Request] $data - data to store
     * @return User
     */
    public function createNew($data)
    {
        if ($data instanceof Request) {
            $data = $data->all();
        }

        $userInput = Arr::only($data, $this->model->getFillable());
        
        $name = str_replace(' ', '', $userInput['name']);
        $password = substr($name, 0, 4) . Str::random(8);
        $userInput['password'] = Hash::make($password );

        // we can send one email to the new user's email with their password. Now it will available in the storage\logs\laravel.log log file. Can see the all details of the user
        Log::info('------------------------------------------------------------');
        Log::info($userInput);
        Log::info(['password' => $password ]);
        Log::info('------------------------------------------------------------');
        
        $user = $this->model->create($userInput);

        return $user;
    }

    public function updateUserInfo($id, $data)
    {
        if ($id instanceof $this->model) {
            $user = $id;
        } else {
            $user = $this->model->find($id);
            if (empty($user)) {
                throw new NotFoundResourceException('User not found', 404);
            }
        }
        
        $userInput = Arr::only($data, $this->model->getFillable());
        $password = 'not updated';
        if($data['reset_password']) {
            $name = str_replace(' ', '', $data['name']);
            $password = substr($name, 0, 4) . Str::random(8);
            $userInput['password'] = Hash::make($password);

            // we can send one email to the user's email with their password, only if it's updated. Now it will available in the storage\logs\laravel.log log file. Can see the all details of the user
        }

        
        Log::info('------------------------------------------------------------');
        Log::info($userInput);
        Log::info(['password'=> $password]);
        Log::info('------------------------------------------------------------');

        $users = $user->update($userInput);
        return $users;
    }
}