<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = now();
        $userList = array(
            [
                'name' => 'Admin',
                'email' => 'admin@usermanagements.com',
                'password' => Hash::make('P@ssword#Admin'),
                'status' => '1',
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
            ]
        );
        Schema::disableForeignKeyConstraints();
        Admin::truncate();
        Schema::enableForeignKeyConstraints();
        Admin::insert($userList);
    }
}
