<?php


return [
    'user_name'     => 'नाम',
    'user_email'    => 'ईमेल',
    'user_status'   => 'दर्जा',
    'create_user'   => 'बनाएं',
    'active_user'   => 'सक्रिय',
    'inactive_user' => 'निष्क्रिय',
    "full_name"     => "उपयोगकर्ता का पूरा नाम",
    "placeholder_email" => "उपयोगकर्ता का ईमेल",
    'back_to_list'  => 'सूची पर वापस जाएं',
    'dashboard'     => 'डैशबोर्ड',
    'create_user_form'   => 'उपयोगकर्ता बनाइये',
    'reset_password'    => 'क्या आप पासवर्ड रीसेट करना चाहते हैं?',
    'yes'   => 'हाँ',
    'no'    => 'नहीं',
    'something_went_wrong' => 'कुछ गलत हो गया!!! कृपया पुन: प्रयास करें',
    'user_not_deleted'     => 'उपयोक्ता हटाया नहीं गया!',
    'user_details_updated_successfully' => 'उपयोगकर्ता विवरण सफलतापूर्वक अपडेट किया गया!',
    'user_deleted_successfully' => 'उपयोगकर्ता सफलतापूर्वक हटा दिया गया!',
    'user_data_not_found' => 'उपयोगकर्ता डेटा नहीं मिला!',
    'user_created_successfully' => 'उपयोगकर्ता सफलतापूर्वक बनाया गया!',
    'delete_confirmation' => 'क्या आप इस उपयोगकर्ता को हटाना चाहते हैं?',
    'user_list' => "उपयोगकर्ता की सूची",
    'search_by' => 'खोज से',
    'search'    => 'खोज',
    'all_status' => 'सभी स्थिति',
    'reset' => 'रीसेट',
    'show'  => 'दिखाना',
    'entries'   => 'प्रविष्टियां',
    'action'    => 'कार्य',
    'update_user' => 'अद्यतन',
    'logout'    => 'लॉग आउट',
    'welcome_user' => "उपयोगकर्ता के डैशबोर्ड में आपका स्वागत है !!"
];

?>