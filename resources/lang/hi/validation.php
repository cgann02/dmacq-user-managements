<?php


return [
    'required' => ':attribute क्षेत्र की आवश्यकता है।',
    'exists' => 'चयनित :attribute अमान्य है।',
    'in' => 'चयनित :attribute अमान्य है।',
    'email' => ':attribute एक मान्य ईमेल पता होना चाहिए।',
];

?>