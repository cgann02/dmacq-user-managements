<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ये क्रेडेंशियल्स हमारे रिकॉर्ड से मेल नहीं खाते हैं।',
    'incorrect_password' => 'प्रदान किया गया पासवर्ड गलत है।',
    'throttle' => 'बहुत सारे लॉगिन प्रयास। कृपया :seconds सेकंड में पुन: प्रयास करें',

    'validation' => [
        'inactive' => 'दर्ज ईमेल निष्क्रिय है। आगे की प्रक्रिया के लिए कृपया अपने व्यवस्थापक से संपर्क करें',
    ],

    'login' => 'लॉग इन करें',
    'email' => 'ईमेल',
    'password' => 'पासवर्ड',
    'user' => 'उपयोगकर्ता',
    'admin' => 'व्यवस्थापक'

];
