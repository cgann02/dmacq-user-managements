<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'incorrect_password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'validation' => [
        'inactive' => 'Entered email is inactive. Please contact your admin for futher process',
    ],

    'login' => 'Login',
    'email' => 'E-mail',
    'password' => 'Password',
    'user' => 'User',
    'admin' => 'Admin'

];
