<?php


return [
    'field_required'=> 'This field is required.',
    'valid_email'   => 'Please enter a valid email address.',
    'value_in'      => 'The selected value is invalid.',
    'value_exists'  => 'Invalid credentials. Please try with valid credentials',
    'value_unique'  => 'The value has already been taken.'
];

?>