<?php


return [
    'user_name'     => 'Name',
    'user_email'    => 'Email',
    'user_status'   => 'Status',
    'create_user'   => 'Create',
    'active_user'   => 'Active',
    'inactive_user' => 'Inacive',
    "full_name"  => "User's full name",
    "placeholder_email"      => "User's email",
    'back_to_list'  => 'Back to list',
    'dashboard'     => 'Dashboard',
    'create_user_form'   => 'Create User',
    'reset_password'    => 'Do you want to reset password ?',
    'yes'   => 'Yes',
    'no'    => 'No',
    'something_went_wrong' => 'Something went wrong!!! Please try again',
    'user_not_deleted'     => 'User not deleted!',
    'user_details_updated_successfully' => 'User Details Updated Successfully!',
    'user_deleted_successfully' => 'User deleted successfully!',
    'user_data_not_found' => 'User data not found!',
    'user_created_successfully' => 'User Created Successfully!',
    'delete_confirmation' => 'Do you want to delete this user?',
    'user_list' => "User's List",
    'search_by' => 'Search by',
    'search'    => 'Search',
    'all_status' => 'All Status',
    'reset' => 'Reset',
    'show'  => 'Show',
    'entries'   => 'Entries',
    'action'    => 'Action',
    'update_user' => 'Update',
    'logout'    => 'Logout',
    'welcome_user' => "Welcome to user's dashboard!!"
];

?>