<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'incorrect_password' => 'La contraseña proporcionada es incorrecta.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',

    'validation' => [
        'inactive' => 'El correo electrónico ingresado está inactivo. Póngase en contacto con su administrador para continuar con el proceso.',
    ],

    'login' => 'Acceso',
    'email' => 'Correo electrónico',
    'password' => 'Contraseña',
    'user' => 'Usuario',
    'admin' => 'Administrador'

];
