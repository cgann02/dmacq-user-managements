<?php


return [
    'required' => 'El campo :attribute es obligatorio.',
    'exists' => 'El :attribute seleccionado no es válido.',
    'in' => 'El :attribute seleccionado no es válido.',
    'email' => 'El :attribute debe ser una dirección de correo electrónico válida.',
];

?>