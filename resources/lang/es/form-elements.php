<?php


return [
    'user_name'     => 'Nombre',
    'user_email'    => 'Correo electrónico',
    'user_status'   => 'Estado',
    'create_user'   => 'Crear',
    'active_user'   => 'Activo',
    'inactive_user' => 'Inactivo',
    "full_name"     => "Nombre completo del usuario",
    "placeholder_email"      => "correo electrónico del usuario",
    'back_to_list'  => 'volver a la lista',
    'dashboard'     => 'Panel',
    'create_user_form'   => 'Crear usuario',
    'reset_password'    => '¿Quieres restablecer la contraseña?',
    'yes'   => 'Sí',
    'no'    => 'No',
    'something_went_wrong' => '¡¡¡Algo salió mal!!! Inténtalo de nuevo',
    'user_not_deleted'     => 'Usuario no eliminado!',
    'user_details_updated_successfully' => '¡Detalles de usuario actualizados con éxito!',
    'user_deleted_successfully' => '¡Usuario eliminado con éxito!',
    'user_data_not_found' => '¡Datos de usuario no encontrados!',
    'user_created_successfully' => 'Usuario creado con éxito!',
    'delete_confirmation' => '¿Quieres eliminar a este usuario?',
    'user_list' => "Lista de usuarios",
    'search_by' => 'Búsqueda por',
    'search'    => 'Buscar',
    'all_status' => 'Todo el estado',
    'reset' => 'Reiniciar',
    'show'  => 'Espectáculo',
    'entries'   => 'Entradas',
    'action'    => 'Acción',
    'update_user' => 'Actualizar',
    'logout'    => 'Cerrar sesión',
    'welcome_user' => "¡Bienvenido al panel de control del usuario!"
];

?>