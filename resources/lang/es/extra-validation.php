<?php


return [
    'field_required'=> 'Este campo es obligatorio.',
    'valid_email'=> 'Por favor, introduce una dirección de correo electrónico válida.',
    'value_in' => 'El valor seleccionado no es válido.',
    'value_exists' => 'Credenciales no válidas. Intente con credenciales válidas.',
    'value_unique'  => 'El valor ya ha sido tomado.'
];

?>