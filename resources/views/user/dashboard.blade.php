@extends('layouts.app')
@section('pageTitle','Dashboard')
@push('css')
<style>

.user-dashboard {
    margin-top: -800px !important;
    margin-right: 5px !important;
    font-weight: 700;
    font-size: 70px;
}
a {
    width: 249px !important;
    color: #e90f0f !important;
    font-size: 50px;
    font-weight: 400;


}
</style>

@endpush
@section('content')
    <div class="user-dashboard">
    {{__('form-elements.welcome_user')}}
    </div>    
    <a href="{{ route('user.logout') }}">
        {{ __('form-elements.logout')}}
    </a>
@endsection