<script src="{{ asset('/vendor/jquery/jquery-3.2.1.min.js') }}"></script>

<script src="{{ asset('/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('/vendor/select2/select2.min.js') }}"></script>

<script src="{{ asset('/vendor/tilt/tilt.jquery.min.js') }}"></script>
<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

<script src="{{ asset('/js/main.js') }}"></script>
<script defer src="https://static.cloudflareinsights.com/beacon.min.js/vaafb692b2aea4879b33c060e79fe94621666317369993" integrity="sha512-0ahDYl866UMhKuYcW078ScMalXqtFJggm7TmlUtp0UlD4eQk0Ixfnm5ykXKvGJNFjLMoortdseTfsRT8oCfgGA==" data-cf-beacon='{"rayId":"79a45aa50dbf85bd","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2023.2.0","si":100}' crossorigin="anonymous"></script>
<script src="{{ asset('js/sweetalert2@11.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
  var something_went_wrong  = "{{__('form-elements.something_went_wrong')}}",
      user_not_deleted      = "{{__('form-elements.user_not_deleted')}}",
      delete_confirmation   = "{{__('form-elements.delete_confirmation')}}";

</script>
<script src="{{ asset('js/admin/trans-language.js')}}"></script>
@stack('js')