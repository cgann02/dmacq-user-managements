<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.header')
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="col-12 trans-div">
                    @php
                        $available_locales  = config('app.available_locales');
                        $current_locale     = session()->get('locale') ?: app()->getLocale();
                    @endphp
                    <select class="form-select form-select-sm" id="trans-lang" aria-label=".form-select-sm example">
                        @foreach($available_locales as $locale_name => $available_locale)
                            @php $selected = ''; @endphp
                            @if($available_locale === $current_locale)
                                @php $selected = 'selected'; @endphp
                            @endif
                                <option value="{{ $available_locale }}" {{$selected}}>{{ $locale_name }}</option>
                        @endforeach
                    </select>
                    
                </div>

                <div class="col-md-12">
                
                    @if (session('commonError'))
                        <div class="alert alert-warning alert-dismissible text-white" role="alert">
                            <span class="text-sm">{{ session('commonError') }}</span>
                        </div>
                    @endif
                    <div class="alert alert-danger alert-dismissible text-white" role="alert" style="display: none;">
                        <span class="text-sm">
                            A simple danger alert with an example link. Give it a click if you like.
                        </span>
                    </div>
                    <div class="alert alert-success alert-dismissible text-white" role="alert" style="display: none;">
                        <span class="text-sm">
                            A simple danger alert with an example link. Give it a click if you like.
                        </span>
                    </div>
                </div>
                @yield('content')
            </div>
        </div>
        
    </body>
    @include('layouts.footer-js')
</html>
