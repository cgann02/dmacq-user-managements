<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="{{ asset('/images/icons/favicon.ico') }}" />

        <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }} | @yield('pageTitle')</title>

        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}"> -->
        <link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />

        <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/animate/animate.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/select2/select2.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('/css/util.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}">

        <meta name="robots" content="noindex, follow">
        <style>
            .alert {
                width: 550px;
                float: right;
            }
            .alert-success {
                background-color: #155724 !important;
                border-color: #155724 !important;
            }
            .alert-danger {
                background-color: #721c24 !important;
                border-color: #721c24 !important;
            }

            .alert-warning {
                background-color: #dbbd5c !important;
                border-color: #dbbd5c !important;
            }

            
        </style>
        
        @stack('css')

  </head>
