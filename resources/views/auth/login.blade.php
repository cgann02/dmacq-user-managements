@extends('layouts.app')
@section('pageTitle','Login')
@push('css')
<style>
.wrap-login100 {
    padding: 53px 130px 33px 95px !important;
}
input[type="radio"] {
    margin-left: 15% !important;
}

.invalid-feedback, .error {
    display: block !important;
    font-size: 13px !important;
    margin-left: 20px !important;
    color: #c80000 !important;
}
</style>
@endpush
@section('content')
                <div class="wrap-login100">
                    <div class="login100-pic js-tilt" data-tilt>
                        <img src="images/img-01.png" alt="IMG">
                    </div>
                    <form class="login100-form validate-form" id="login-form" method="POST" action="{{ route('submit-login') }}">
                        <span class="login100-form-title">
                        {{ __('auth.login') }}
                        </span>
                        {{ csrf_field() }}
                        <div class="wrap-input100 validate-input">
                            <input class="input100" type="text" name="email" placeholder="{{ __('auth.email') }}" value="{{ old('email') }}">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="wrap-input100 validate-input">
                            <input class="input100" type="password" name="password" value="{{ old('password') }}" placeholder="{{ __('auth.password') }}">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="wrap-input100 validate-input">
                            <br>
                            <input class="radio" type="radio" name="role" value="user" checked> {{ __('auth.user') }} 
                            <input class="radio" type="radio" name="role" value="admin" @if(old('role') == 'admin') checked @endif> {{ __('auth.admin') }}
                            
                        </div>
                        @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn">
                            {{ __('auth.login') }}
                            </button>
                        </div>
                        <div class="container-login100-form-btn">
                        </div>
                        <div class="container-login100-form-btn">
                        </div>
                    </form>
                </div>
@endsection

@push('js')
    <script>
        var field_required = "{{ __('extra-validation.field_required')}}",
            invalid_email = "{{ __('extra-validation.valid_email')}}";
    </script>
    <script type="text/javascript" src="{{ asset('/js/login.js') }}"></script>
@endpush