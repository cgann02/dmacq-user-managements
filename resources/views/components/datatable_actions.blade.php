<div>
    <div class="btn-group" role="group" aria-label="Basic example">
        <a class="btn btn-primary btn-sm px-2 py-1" href="{{ route($path, $id) }}" data-original-title="" title="Edit">
            <span class="fa fa-edit" style="font-size:1rem"></span>
        </a>   
        <a class="btn btn-danger btn-sm px-2 py-1 delete-item js-delete-btn" href="javascript:void(0);" data-id="{{ $id }}" data-original-title="" title="Delete">
            <span class="fa fa-trash" style="font-size:1rem"></span>
        </a>
    </div>
</div>

