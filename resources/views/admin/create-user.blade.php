@extends('layouts.app')
@section('pageTitle','Create User')
@push('css')
<style>

    .wrap-login100 {
        width: 1203px !important;
        height: 770px;

        padding: 24px 130px 33px 95px !important;
    }

    .table-body {
        width: 1203px !important;
    }

    #user-details-datatable {
        width: 100% !important;
    }

    .bg-gradient-success {
        color: green !important;
    }

    .bg-gradient-secondary {
        color: red !important;
    }

    .form-body {
        box-sizing: border-box;
        width: 100%;
        min-height: 88%;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        /* align-items: center; */
        /* padding: 15px; */
        background: #e9ecef;
        margin-left: 25%;
        
    }

    form {
        width: 100%;
        padding: 28px 8px 8px 61px;
    }

    .common-success {
        text-align: right;
        margin-bottom:5px;
    }

    .col-md-12 {
        margin-top: 8px !important;
    }

    label {
        margin-bottom: 0 !important;
    }
    .create-btn {
        margin-top: 50px;
        background-color: #007bff;
        border-color: #007bff;
    }

    input[type="radio"] {
        margin-left: 50px !important ;
        margin-right: 5px !important ;
    }

    .invalid-feedback {
        margin-left: 25px;
    }
</style>

@endpush
@section('content')
    <div class="wrap-login100">
            <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0 table-body">
                    <!-- <span class="app-name">Users List</span> -->
                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-6" style="text-align: left;">
                            <a href="{{ route('admin.dashboard') }}" class="btn btn-info"><i class="fa fa-home" aria-hidden="true"></i> {{ __('form-elements.dashboard') }}</a>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <a href="{{ route('admin.users-list') }}" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('form-elements.back_to_list') }}</a>
                        </div>
                    </div>
                    <div class="form-body col-6">
                        <form class="" id="create-or-update-user" data-url="{{ route('admin.store-user') }}">
                        {{ csrf_field() }}
                            <div class="row" style="width: 100% !important;">
                                    <div class="col-md-12 data-field">
                                        <label class="form-label" for="">{{ __('form-elements.user_name')}}: </label>
                                        <input type="text" class="form-control" name="name" placeholder="{{ __('form-elements.full_name') }}" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32)">
                                        
                                    </div>
                                    
                                    <div class="col-md-12 data-field">
                                        <label class="form-label" for="">{{ __('form-elements.user_email') }}: </label>
                                        <input type="text" class="form-control" name="email" placeholder="{{ __('form-elements.placeholder_email') }}">
                                    </div>

                                    <div class="col-md-12 data-field">
                                        <label class="form-label">{{ __('form-elements.user_status') }}: </label>
                                        <div class="input-group input-group-outline">
                                            <input type="radio" class="radio" id="active" name="status" value="1" checked><label for="active">{{ __('form-elements.active_user') }}</label>
                                            <input type="radio" class="radio" id="inactive" name="status" value="0"><label for="inactive">{{ __('form-elements.inactive_user') }}</label>

                                        </div>
                                    </div>

                                    <div class="text-center col-12">
                                        <button type="submit" class="btn btn-info create-btn">{{ __('form-elements.create_user') }}</button>
                                    </div>
                                </div>

                        </form>
                    </div>
            </div>

    </div>
@endsection

@push('js')

<script>
    var field_required = "{{ __('extra-validation.field_required')}}",
        invalid_email = "{{ __('extra-validation.valid_email')}}";
</script>

<script src="{{ asset('js/admin/create-or-update-user.js')}}"></script>



@endpush