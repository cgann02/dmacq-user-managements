@extends('layouts.app')
@section('pageTitle','Dashboard')
@push('css')
<style>

.wrap-login100 {
    width: 269px !important;
    padding: 50px 50px 50px 75px!important;
    color: #d722d7;
    font-weight: 700;
}

.col-md-123 {
    margin-top: -500px !important;
    margin-right: 5px !important;
}

</style>

@endpush
@section('content')
    <div class="col-md-123">
        <a href="{{ route('admin.users-list') }}">
            <div class="wrap-login100">
                {{ __('form-elements.user_list')}}

            </div>
        </a>
    </div>                  
    <div class="col-md-123">
        <a href="{{ route('admin.create-user') }}">
            <div class="wrap-login100">
                {{ __('form-elements.create_user_form')}}

            </div>
        </a>
    </div>
    <div class="col-md-123">
        <a href="{{ route('admin.logout') }}">
            <div class="wrap-login100">
                {{ __('form-elements.logout')}}

            </div>
        </a>
    </div>

@endsection