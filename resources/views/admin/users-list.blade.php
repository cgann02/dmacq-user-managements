@extends('layouts.app')
@section('pageTitle','User List')
@push('css')
<style>

    .wrap-login100 {
        width: 1203px !important;
        height: 770px;
        padding: 24px 130px 33px 95px !important
    }

    .table-body {
        width: 1203px !important;
    }

    #user-details-datatable {
        width: 100% !important;
    }

    .bg-gradient-success {
        color: green !important;
    }

    .bg-gradient-secondary {
        color: red !important;
    }
</style>

@endpush
@section('content')
    <div class="wrap-login100">
            <div class="relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0 table-body">
                    <!-- <span class="app-name">Users List</span> -->
                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-6" style="text-align: left;">
                            <a href="{{ route('admin.dashboard') }}" class="btn btn-info"><i class="fa fa-home" aria-hidden="true"></i> {{ __('form-elements.dashboard') }}</a>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <a href="{{ route('admin.create-user') }}" class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('form-elements.create_user_form') }}</a>
                        </div>
                    </div>

                    <div class="card-header">
                        <form class="" id="filter-form">
                            <!-- <label class="mr-sm-2 form-label" for="admin-user-search">Search by:</label>
                            <input type="text" class="form-control search mb-2 mr-sm-2 mb-sm-0" id="filter-search"
                                placeholder="Search ..."> -->

                            <div class="row" style="width: 100% !important;">
                                <div class="col-md-4">
                                    <label class="form-label" for="">{{ __('form-elements.search_by') }}: </label>
                                    <input type="text" class="form-control search mb-2 mr-sm-2 mb-sm-0" id="filter-search" placeholder="{{ __('form-elements.search') }} ...">
                                </div>
                                
                                <div class="col-md-4">
                                    <label class="form-label" for="admin-user-search">{{ __('form-elements.user_status') }} </label>
                                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter-status">
                                        <option value="">{{ __('form-elements.all_status') }}</option>

                                        <option value="1">{{ __('form-elements.active_user') }}</option>
                                        <option value="0">{{ __('form-elements.inactive_user') }}</option>

                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- DataTable init -->
                    <div class="table-responsive">
                        
                        @component('components.datatable',compact('dataTable')) @endcomponent
                    </div>
            </div>

    </div>
@endsection

@push('js')
<script>
    const userDeleteURL = "{{ route('admin.delete-user') }}"; 
</script>
<script src="{{ asset('js/admin/user-list.js')}}"></script>

@endpush