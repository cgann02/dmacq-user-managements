## clone and setup steps through terminal

=> Copy the URL from git https://gitlab.com/cgann02/dmacq-user-managements.git (Clone with HTTPS)
=> run the following commands in terminal
    -> go to the project setup folder (htdocs - if your using XAMPP)

    -> open the command prompt 
    
    -> git clone https://gitlab.com/cgann02/dmacq-user-managements.git (. -> add dot(.) if you want to clone the project in a fresh folder which is already created).

    -> fetch all the data from the main branch

        git pull origin main
    
    -> Then checkout to the develop branch

        git checkout develop

    -> Again pull all the data from the develop branch ( May be some changes are available in develop branch, which is in under verification). and it's better to avoid conflicts with others.

        git pull origin develop

    -> Create a new work branch from the branch develop for each person,

        git checkout -b (new-branch-name)

        Ex:- git checkout -b admin-panel-crud

        This one is a good practice to maintain the workflow and keeping the parent folder error free

    -> copy the .env.example file and rename it as .env

    -> create one database for the project and  replace the 
        DB_CONNECTION=
        DB_HOST=
        DB_PORT=
        DB_DATABASE=
        DB_USERNAME=
        DB_PASSWORD=

        details with your current database details
    
    -> install the compposer through the command, 

        composer install 

    -> generate the application key,

        php artisan key:generate
    
    -> Now the system is ready to run

        php artisan serve

        You can find the running application http://127.0.0.1:8000 with this URL.

