setTimeout( function() {
    $('.invalid-feedback').remove();
}, 5000);

$(function() {
    $("#login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            },
            role: {
                required: true
            }
        },
        messages: {
            password: {
                required: field_required
            },
            email: {
                required: field_required,
                email: invalid_email//"Please enter a valid email address"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            var n = element.attr("name");

            error.addClass('error');
            element.parent('.validate-input').append(error);
        },
        // highlight: function (element, errorClass, validClass) {
        //     $(element).addClass('is-invalid');
        // },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('error');
            $(element).parent().find('span.error').remove();
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});