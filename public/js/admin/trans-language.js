$('#trans-lang').on('change', function() {

    $.ajax({
        type: "GET",
        url: '/language/' + $(this).val(),
        dataType: "json",
        headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content") },
        success : function() {
            location.reload();
        }
    });
});
