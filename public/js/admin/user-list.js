$(document).on('click', '.js-delete-btn', function() {
    let _this = $(this),
        user_id = _this.attr('data-id');
    if(user_id) {
        Swal.fire({
            title: delete_confirmation,
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({

                    type : "GET",
                    url : userDeleteURL + '/' + user_id,
                    processData: false,
                    contentType: false,
                    dataType: 'json',           
        
                    success :function(result){
                        if(result.success) {
                            Swal.fire(result.message, '', 'success')
                            $('.alert-success span:first').html(result.message);
                            $('.alert-success').show();

                            $('#user-details-datatable').DataTable().ajax.reload();
                        } else {
                            Swal.fire(result.message, '', 'error')
                            $('.alert-danger span:first').html(result.message);
                            $('.alert-danger').show();
                            $('#user-details-datatable').DataTable().ajax.reload();
                        }
                    },
                    error: function(response) {
                        Swal.fire(response.responseJSON.message, '', 'error')
                    },
                    complete: function() {
                        setTimeout( function () {
                            $('.alert').hide();
                        }, 2000 );
                    }
                });
            } else if (result.isDismissed) {
              Swal.fire(user_not_deleted, '', 'info')
            }
        })
    }

});
