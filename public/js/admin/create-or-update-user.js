$(document).ready(function () {

    setTimeout( function() {
        $('.alert').hide();
    }, 5000);

    $('input, select').on('change', function() {
        $(this).removeClass('is-invalid');
        $(this).siblings('span.invalid-feedback').hide();
    });

    $("#create-or-update-user").submit(function(e){
        e.preventDefault();

        $('div.alert, span .error').hide();

        if(!$(this).valid()) {
            return false;
        }

        var myform = document.getElementById("create-or-update-user");
        var fd = new FormData(myform);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: $(this).attr('data-url'),
            dataType: 'JSON',
            type: "POST",
            contentType: false,
            processData: false,
            data: fd,
            beforeSend: function() {
                $('div.alert, span.error').hide();
                $('button[type="submit"]').attr('disabled', true);
            },
            success: function (response) {
                if(response.success) {
                    $('.alert-success span:first').html(response.message);
                    $('.alert-success').show();
                }

                setTimeout( function () {
                    location.reload();
                }, 3000 );
            },
            error: function (error) {
                var response = JSON.parse(error.responseText);
                if (response.code == 422) {

                    $.each(response.errors, function (key, value) {
                        $('[name="' + key + '"]').addClass("is-invalid");
                        
                        if($('span[for="' + key + '"]').length) {
                            $('span[for="' + key + '"]').html(value).show();
                        } else {
                            $('<span for="' + key + '" class="error invalid-feedback">' + value + '</span>').insertAfter($('[name="' + key +'"]').closest('.data-field'));

                            $('span[for="' + key + '"]').show();
                        }
                                                        
                    });

                } else {
                    $(".alert-danger span:first").html(something_went_wrong);
                    $(".alert-danger").show();
                }

                
            },
            complete: function() {
                $('button[type="submit"]').removeAttr('disabled');

                setTimeout( function() {
                    $('.alert').hide();
                }, 3000);
            }
        });
    });

    $('#create-or-update-user').validate({
        ignore: "",
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },            
            status: {
                required: true
            },
            reset_password: {
                required: function() {
                    return $('[name="user_id"]').length;
                }
            }
        },
        messages: {
            name: {
                required: field_required
            },
            email: {
                required: field_required,
                email: invalid_email//"Please enter a valid email address"
            },
            status: {
                required: field_required
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            var n = element.attr("name");

            error.addClass('invalid-feedback');

            $(error).insertAfter(element.closest('.data-field'));
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $('input[name="user_name"]').on('keyup', function() {
        var name = $(this).val();
        
        if(name.charAt(0) == ' ') {
            $(this).val($(this).val().trim());
        }
    })
});