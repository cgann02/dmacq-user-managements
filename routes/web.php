<?php

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthenticationController::class, 'login'])->name('login');

Route::post('login', [AuthenticationController::class, 'postLogin'])->name('submit-login');

Route::group(['as' => 'admin.', 'namespace' => 'admin', 'middleware' => 'auth_admin', 'prefix' => 'admin'], function () {

    Route::get('dashboard', [AdminController::class, 'adminDashboard'])->name('dashboard');

    Route::get('users-list', [AdminController::class, 'index'])->name('users-list');

    Route::get('create-user', [AdminController::class, 'getNewUserForm'])->name('create-user');
    Route::post('store-user', [AdminController::class, 'storeUserDetails'])->name('store-user');

    Route::get('edit-user/{id}', [AdminController::class, 'getUpdateUserForm'])->name('edit-user');
    Route::post('update-user', [AdminController::class, 'updateUserDetails'])->name('update-user');

    Route::get('delete-user/{id?}', [AdminController::class, 'deleteUser'])->name('delete-user');

    Route::any('logout', [AdminController::class, 'adminLogout'])->name('logout');
});


Route::group(['as' => 'user.', 'namespace' => 'user', 'middleware' => 'auth_user', 'prefix' => 'user'], function () {

    Route::get('dashboard', [UserController::class, 'userDashboard'])->name('dashboard');

    Route::any('logout', [UserController::class, 'userLogout'])->name('logout');

});

Route::get('language/{locale?}', function ($locale = null) {
    if (isset($locale) && in_array($locale, config('app.available_locales'))) {
        
        App::setLocale($locale);
        Session::put('locale', $locale);

        URL::defaults(['locale' => $locale]);

        Artisan::call('optimize:clear');
        return true;
    }
    
});